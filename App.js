import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {Button} from 'react-native-paper';
import {Calendar} from 'react-native-calendars';
import Mdl from './Mdl';
import Pro from './Pro';
import Uber from './Uber';
import Intro from './Intro';

class App extends Component{
  constructor(){
    super();
    this.state = {data:{}};
  }
  createData(){
    let obj = {};
      // '2019-08-25': {selected: true, marked: true, selectedColor: 'blue'}
    for(let i = 2019; i<=3000;i++){
        obj[i+'-08-15'] = {selected: true, marked: true, selectedColor: 'blue'};
    }
    this.setState({data:obj});
  }
  componentWillMount(){
    this.createData();
  }
  daycheck(day){
    if(day.month==8 && day.day==15){
      alert('Jai Bharat');
    }
  }
  render(){
    return(
      <View style={{flex:1, marginTop:40}}>
        <Calendar 
            minDate={'2019-01-01'}
            maxDate={'2020-05-30'}
            markedDates={this.state.data}
            onDayPress={(day) => alert('selected day'+ day.day)}
            onDayLongPress={(day) => this.daycheck(day)}
        />
      </View>
    )
  }
}
export default Mdl;