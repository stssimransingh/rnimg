import React, { Component } from 'react';
import { View, Text , Button, StatusBar} from 'react-native';
import Modal from "react-native-modal";
import User from './User';
class Mdl extends Component {
  constructor(props) {
    super(props);
    this.state = {
        showmodal:false
    };
  }
  mymodal(){
      if(this.state.showmodal){
          this.setState({showmodal:false});
      }else{
         this.setState({showmodal:true});
      }
  }
  render() {
    return (
      <View style={{flex:1, marginTop:40}}>
          <StatusBar backgroundColor="green" barStyle="dark-content" />
          <Button
            title="Click Me"
            onPress={() => this.mymodal()}
          />
         <Modal
            isVisible={this.state.showmodal} style={{backgroundColor:'white'}}
            onSwipeComplete={() => this.setState({ showmodal: false })}
            swipeDirection="right"
            animationIn="slideInLeft"
            animationInTiming={2000}
            animationOut="slideOutRight"
            animationOutTiming={2000}
            scrollHorizontal={true}
         >
          <View style={{}}>
            <User name="James Bond" details="This is my Text" designation="Web developer" color="red" image="https://artofidealism.files.wordpress.com/2012/01/person-pawn-nitichan-_ubuntu.jpg"/>
            <Button 
                title="Close"
                onPress={() => this.mymodal()}
            />
          </View>
        </Modal>
      </View>
    );
    }
}

export default Mdl;
