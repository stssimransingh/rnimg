import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Canvas from 'react-native-canvas';
class Can extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  handleCanvas = (canvas) => {
    const ctx = canvas.getContext('2d');
    ctx.fillStyle = 'purple';
    ctx.fillRect(0, 0, 100, 100);
  }

  render() {
    return (
      <Canvas ref={this.handleCanvas}/>
    )
  }
}

export default Can;
