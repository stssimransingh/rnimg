import React, { Component } from 'react';
import { View, Text, ScrollView, Dimensions , StyleSheet, Image} from 'react-native';
import { Container, Content, Header } from 'native-base';
import AppIntroSlider from 'react-native-app-intro-slider';
const slides = [
    {
      key: 'somethun',
      title: 'Title 1',
      text: 'Description.\nSay something cool',
      image: {uri:'http://cdn.onlinewebfonts.com/svg/img_106504.png'},
      backgroundColor: '#59b2ab',
    },
    {
      key: 'somethun-dos',
      title: 'Title 2',
      text: 'Other cool stuff',
      image: {uri:'http://cdn.onlinewebfonts.com/svg/img_106504.png'},
      backgroundColor: '#febe29',
    },
    {
      key: 'somethun1',
      title: 'Rocket guy',
      text: 'I\'m already out of descriptions\n\nLorem ipsum bla bla bla',
      image: {uri:'http://cdn.onlinewebfonts.com/svg/img_106504.png'},
      backgroundColor: '#22bcb5',
    }
  ];
class Intro extends Component {
    constructor(){
        super();
        this.state = {
            showRealApp: false
        }
    }
    _onDone(){
        alert('good');
    }
    _renderItem = (item) => {
        return (
          <View style={styles.slide}>
            <Text style={styles.title}>{item.title}</Text>
            <Image source={item.image} />
            <Text style={styles.text}>{item.text}</Text>
          </View>
        );
      }    
    render() {
        if(this.state.showRealApp){
        return (
            <View style={{flex:1, backgroundColor:'green'}}>
                <Text>This is it</Text>
            </View>
        );
        }else{
            return(
                <AppIntroSlider renderItem={this._renderItem} slides={slides} onDone={this._onDone}/>
            )
        }
    }
}
const styles = StyleSheet.create({
    mainContent: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'space-around',
    },
    image: {
      width: 320,
      height: 320,
    },
    text: {
      color: 'rgba(255, 255, 255, 0.8)',
      backgroundColor: 'transparent',
      textAlign: 'center',
      paddingHorizontal: 16,
    },
    title: {
      fontSize: 22,
      color: 'white',
      backgroundColor: 'transparent',
      textAlign: 'center',
      marginBottom: 16,
    },
  });
  

export default Intro;
