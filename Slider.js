import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { SliderBox } from 'react-native-image-slider-box';
class Slider extends Component {
  constructor(props) {
    super(props);
    this.state = {
        images: [
            'https://source.unsplash.com/1024x768/?nature',
            'https://source.unsplash.com/1024x768/?water',
            'https://source.unsplash.com/1024x768/?girl',
            'https://source.unsplash.com/1024x768/?tree'
          ]
    };
  }

  render() {
    return (
      <View style={{flex:1}}>
        <SliderBox images={this.state.images} 
        onCurrentImagePressed={index =>
            alert(index)
        }
        sliderBoxHeight={400}
        dotColor="#FFEE58"
        inactiveDotColor="#90A4AE"
        circleLoop
       
        />
      </View>
    );
  }
}

export default Slider;
