import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import {Container, Content, Header,Card, CardItem, Body } from 'native-base';
class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
    
    <Card>
        <CardItem bordered style={{backgroundColor:this.props.color}}>
          <Text>{this.props.name}</Text>
        </CardItem>
        <CardItem bordered>
          <Body>
              <View style={{width:100, height:100, borderRadius:50, backgroundColor:this.props.color, alignSelf:'center'}}>
                <Image 
                   source={{uri:this.props.image}} 
                   style={{flex:1, borderRadius:50}}
                />
                
              </View>
              <View style={{width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderLeftWidth: 50,
    borderRightWidth: 50,
    borderBottomWidth: 100,
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: 'red'}} />
            <Text>
              {this.props.details}
            </Text>
          </Body>
        </CardItem>
        <CardItem bordered>
          <Text>{this.props.designation}</Text>
        </CardItem>
      </Card>
    );
  }
}

export default User;
